package org.joserivera;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeAwsTestsIT extends AwsTestsTest {

    // Execute the same tests but in native mode.
}